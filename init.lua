-- vkbd - A virtual keyboard for the awesome window manager

local wibox = require("wibox")
local awful = require("awful")

local vkbd = {
  func_down     = {},
  homogeneous   = true,
  spacing       = 5,
  min_cols_size = 10,
  min_rows_size = 10,
  expand        = true,
  layout        = wibox.layout.grid,
  font          = "sans 30",
  smallfont     = "sans 20"
}

local vkbd_widget = wibox.widget(vkbd)

-- Returns a function that handles function keys
function func_key(char)
  if(char == 'Shift_L' or char == 'Shift_R') then
    -- Special handling for shift: Change keymap
    local function shift()
      for _,key in pairs (vkbd_widget:get_all_children()) do
        key.text = key.shifted
      end
      vkbd.func_down[char] = char
    end
    return shift
  elseif (char == 'ISO_Level3_Shift') then
    -- Special handling for altgr: Change keymap
    local function altgr()
      for _,key in pairs (vkbd_widget:get_all_children()) do
        key.text = key.altgr
      end
      vkbd.func_down[char] = char
    end
    return altgr
  else
    -- Regular handling for all other keys
    local function fun ()
      vkbd.func_down[char] = char
    end
    return fun
  end
end

-- Returns a function that handles regular keys
function easy_key(char)
  local function fun ()
    -- Press all function keys
    for _,func in pairs(vkbd.func_down) do
      root.fake_input('key_press', func)
    end

    -- Press and release the actual key
    root.fake_input('key_press',   char)
    root.fake_input('key_release', char)

    -- Release all function keys
    for _,func in pairs(vkbd.func_down) do
      root.fake_input('key_release', func)
    end

    -- Unshift all key labels
    for _,key in pairs (vkbd_widget:get_all_children()) do
      key.text = key.unshifted
    end

    -- Clear all function keys
    vkbd.func_down = {}
  end
  return fun
end

-- Creates a button with the given text labels and function
local function kbd_key(unshifted, shifted, altgr, fun)
  local result = wibox.widget({
    {
      text = unshifted,
      unshifted = unshifted,
      shifted = shifted,
      altgr = altgr,
      font = math.max(unshifted:len(),shifted:len(),altgr:len()) < 3 and vkbd.font or vkbd.smallfont,
      align  = 'center',
      valign = 'center',
      ellipsize = 'none',
      widget = wibox.widget.textbox
    },
    bg='#000000',
    widget = wibox.container.background
  })
  result:buttons(awful.util.table.join(awful.button({ }, 1, fun)))
  return result
end

-- Generates all the keys from a keymap
function vkbd_widget.init(map)
  local keymap = require("vkbd/keymaps/"..map)
  for _,kbdkey in ipairs(keymap) do
    vkbd_widget:add_widget_at(kbd_key(kbdkey[1], kbdkey[2], kbdkey[3], kbdkey[4]), kbdkey[5], kbdkey[6], kbdkey[7], kbdkey[8])
  end
end

return vkbd_widget
